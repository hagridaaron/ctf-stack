FROM kalilinux/kali-rolling
VOLUME /wordlists
VOLUME /ctf

RUN apt-get update && apt-get upgrade -y
# Kali Tools
RUN apt-get install -y nmap john dirb wget curl binwalk foremost libimage-exiftool-perl exiv2 binutils steghide ruby-dev vim kali-linux-core kali-linux-headless kali-linux-nethunter kali-tools-information-gathering kali-tools-vulnerability kali-tools-web kali-tools-database kali-tools-passwords kali-tools-wireless kali-tools-reverse-engineering kali-tools-exploitation kali-tools-social-engineering kali-tools-sniffing-spoofing kali-tools-post-exploitation kali-tools-forensics kali-tools-reporting vim procps iptables openvpn

RUN gem install rake && gem install zsteg nc

ENTRYPOINT [ "/bin/bash" ]